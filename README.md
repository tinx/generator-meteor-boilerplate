# Meteor Boilerplate Generator

## Description

This generator will generate code for the Meteor Boilerplate. The boilerplate is a very simple CMS, created and structured as instructed by Meteor.

- You can find the repository and documentation here: https://bitbucket.org/tinx/meteor-boilerplate.git
- More about meteor: https://www.meteor.com

## Current Meteor version

The used Meteor version is __1.6__.

## Install

- You need npm and node for this. Easy install via NVM, https://github.com/creationix/nvm
- You need Yeoman for this. Installing via `npm install -g yo`
- To use this generator locally, you can just clone this into your local filesystem and use `npm link`

## Documentation

### yo meteor-boilerplate

This will create a fresh meteor boilerplate project.

### yo meteor-boilerplate:addbackendmodule

Adds a backend module. This is a combination of addcollection, definebackendmodule and addbackendoverview.

### yo meteor-boilerplate:addbackendoverview [name] [route]

This will add an overview for a collection. An overview that can also add/update/remove an object in the collection.

* __name__: The name of the collection to add an overview for.
* __route__: The route to the overview.

### yo meteor-boilerplate:addcollection [name]

This will add a meteor collection to the boilerplate.

* __name__: The name of the collection.

### yo meteor-boilerplate:addfrontendpage [name] [route]

This will add a page in the frontend.

* __name__: The name of the page.
* __route__: The route to the page.

### yo meteor-boilerplate:definebackendmodule [name]

This will define a module in the Modules collection and adds a link in the backend sidebar.

* __name__: The name of the module.
'use strict';
var Generator = require('yeoman-generator');
var clone = require('git-clone');
var yosay = require('yosay');

module.exports = class extends Generator {

    prompting() 
    {
        // say something to the user
        this.log(yosay(
            ("Welcome to the Meteor Boilerplate generator.")
        ));

        // prompt something
        return this.prompt([{
            type: 'input',
            name: 'name',
            message: 'Your project name',
            default: 'meteor-boilerplate'
        }]).then((props) => {
            this.props = props;
        });
    }

    install() 
    {
        // clone the repository
        clone("git@bitbucket.org:tinx/meteor-boilerplate.git", process.cwd() + "/" + this.props.name);

        // a message
        this.log("----------------");
        this.log("Do not forget to run 'meteor npm install' before you start the application.");
        this.log("Start your meteor application via 'meteor -p [PORT]'.");   
        this.log("----------------");        
        this.log("You can enter the backend via http://[YOUR_URL]/admin.")
        this.log("User: admin");
        this.log("Password: admin");
        this.log("----------------");
    }

};
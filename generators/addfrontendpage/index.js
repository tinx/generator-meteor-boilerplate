'use strict';
var Generator = require('yeoman-generator');
var fs = require('fs');
var lineNumber = require('linenumber');
var insertLine = require('insert-line');
var chalk = require('chalk');

module.exports = class extends Generator {
    
    constructor(args, opts) 
    {
        super(args, opts);

        try
        {
            this.argument('name', {
                type: String,
                required: true,
                description: 'Page name'
            });

            this.argument('route', {
                type: String,
                required: true,
                description: 'The route'
            });
        }
        catch(ex) 
        {
            this.log(chalk.red('CREATE PAGE...Please provide a valid name and/or route argument.'));
        }
    }
    
    writing() 
    {
        // validate
        if(this.options.name == "" || typeof this.options.name === "undefined") return;

        // get the page information
        var data = {
            pageName: this.options.name.toLowerCase(),
            pageNameUppercase: this.options.name.charAt(0).toUpperCase() + this.options.name.slice(1)
        }

        // the pages path
        var pagesPath = process.cwd() + "/imports/ui/frontend/pages/" + data.pageName;

        /**
         * Creating the page
         */

        // create the page directory
        if (!fs.existsSync(pagesPath)){ fs.mkdirSync(pagesPath); }
        else {  
            this.log(chalk.red('This frontend page already exists.'));
            return;
        }

        // copy html page file
        this.fs.copyTpl(
            this.templatePath('page.html'),
            this.destinationPath(pagesPath + "/" + data.pageName + ".html"),
            data
        );

        // copy javascript page file
        this.fs.copyTpl(
            this.templatePath('page.js'),
            this.destinationPath(pagesPath + "/" + data.pageName + ".js"),
            data
        );

        /**
         * Edit routes file
         */

        // get the frontend routes path
        var frontendRoutesPath = process.cwd() + "/imports/startup/client/routes_frontend.js";
        
        // read the file and find a line        
        var fixture = fs.readFileSync(frontendRoutesPath, 'utf8');        
        var newPageEx = /### IMPORT NEW PAGES REFERER ###/g;
        var newRouteEx = /### IMPORT NEW ROUTES REFERER ###/g;
        var newPagesLine = lineNumber(fixture, newPageEx)[0].line;
        var newRouteLine = lineNumber(fixture, newRouteEx)[0].line;
        
        // when we found the newPagesLine, add text
        if(typeof newPagesLine !== "undefined" && newPagesLine != 0) {
            insertLine(frontendRoutesPath).contentSync("import '../../ui/frontend/pages/" + data.pageName + "/" + data.pageName + ".js';").at(newPagesLine);
        }

        // when we found the newRouteLine, add text
        if(typeof newRouteLine !== "undefined" && newRouteLine != 0) 
        {
            var string = [
                "",
                "/**",
                " * " + data.pageNameUppercase,
                " */",
                "",            
                "frontendRoutes.route('" + this.options.route + "', {",
                "   name: 'Frontend." + data.pageName + "',",
                "   subscriptions() {",
                "       // add subscriptions here",
                "   },",
                "   action() {",
                "       BlazeLayout.render('Frontend_body', { main: '" + data.pageName + "' });",
                "   }",
                "});"
            ];

            // insert line
            insertLine(frontendRoutesPath).contentSync(string.join("\n")).at(newRouteLine);
        }
    }
};
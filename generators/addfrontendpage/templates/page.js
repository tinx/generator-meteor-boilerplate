/**
 * The <%=pageName%> template
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Meteor } from 'meteor/meteor';
import './<%=pageName%>.html';

// on rendered
Template.<%=pageName%>.onRendered(function() {
});

// on created
Template.<%=pageName%>.onCreated(function() {
});

// helpers
Template.<%=pageName%>.helpers({
});

// events
Template.<%=pageName%>.events({
});

// destroyed
Template.<%=pageName%>.onDestroyed(function () {
});
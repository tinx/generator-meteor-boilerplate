'use strict';
var Generator = require('yeoman-generator');
var fs = require('fs');
var lineNumber = require('linenumber');
var insertLine = require('insert-line');
var chalk = require('chalk');

module.exports = class extends Generator {

    constructor(args, opts) 
    {
        super(args, opts);

        try
        {
            this.argument('name', {
                type: String,
                required: true,
                description: 'Collection name'
            });

            this.argument('route', {
                type: String,
                required: true,
                description: 'The route'
            });
        }
        catch(ex) 
        {
            this.log(chalk.red('CREATE OVERVIEW...Please provide a valid collection name and route argument.'));
        }
    }

    writing() 
    {
        // validate
        if(this.options.name == "" || typeof this.options.name == "undefined") return;
        if(this.options.route == "" || typeof this.options.route == "undefined") return;

        // get the collection name
        var collectionName = this.options.name.toLowerCase();
        var collectionSingle = collectionName.slice(0, -1);

        // create the template data
        var data = {
            lcCollection: collectionName,
            ucCollection: collectionName.charAt(0).toUpperCase() + collectionName.slice(1),
            collectionSingle: collectionSingle,
            collectionSingleUppercase: collectionSingle.charAt(0).toUpperCase() + collectionSingle.slice(1)
        }

        // the collection path
        var pagesPath = process.cwd() + "/imports/ui/backend/pages/" + data.lcCollection;
        var componentsPath = process.cwd() + "/imports/ui/backend/components/" + data.lcCollection;
       
        /**
         * Creating the page
         */

        // create the page directory
        if (!fs.existsSync(pagesPath)){ fs.mkdirSync(pagesPath); }
        else {  
            this.log(chalk.red('The page already exists.'));
            return;
        }

        // copy html page file
        this.fs.copyTpl(
            this.templatePath('page.html'),
            this.destinationPath(pagesPath + "/" + data.lcCollection + ".html"),
            data
        );

        // copy javascript page file
        this.fs.copyTpl(
            this.templatePath('page.js'),
            this.destinationPath(pagesPath + "/" + data.lcCollection + ".js"),
            data
        );

        /**
         * Creating the components
         */

        // create the components directories
        if (!fs.existsSync(componentsPath)){ fs.mkdirSync(componentsPath); }
        else {  
            this.log(chalk.red('The components for this page already exist.'));
            return;
        }

        //
        // add modal
        //

        var addModalPath = componentsPath + "/add" + data.collectionSingle;
        if (!fs.existsSync(addModalPath)){ fs.mkdirSync(addModalPath); }

        // copy html add modal component file
        this.fs.copyTpl(
            this.templatePath('addmodal.html'),
            this.destinationPath(addModalPath + "/add" + data.collectionSingle + ".html"),
            data
        );

        // copy javascript page file
        this.fs.copyTpl(
            this.templatePath('addmodal.js'),
            this.destinationPath(addModalPath + "/add" + data.collectionSingle + ".js"),
            data
        );

        //
        // edit modal
        //

        var editModalPath = componentsPath + "/edit" + data.collectionSingle;
        if (!fs.existsSync(editModalPath)){ fs.mkdirSync(editModalPath); }

        // copy html add modal component file
        this.fs.copyTpl(
            this.templatePath('editmodal.html'),
            this.destinationPath(editModalPath + "/edit" + data.collectionSingle + ".html"),
            data
        );

        // copy javascript page file
        this.fs.copyTpl(
            this.templatePath('editmodal.js'),
            this.destinationPath(editModalPath + "/edit" + data.collectionSingle + ".js"),
            data
        );

        //
        // edit routes in backend
        //

        // get the backend routes path
        var backendRoutesPath = process.cwd() + "/imports/startup/client/routes_backend.js";
        
        // read the file and find a line        
        var fixture = fs.readFileSync(backendRoutesPath, 'utf8');        
        var newPageEx = /### IMPORT NEW PAGES REFERER ###/g;
        var newRouteEx = /### IMPORT NEW ROUTES REFERER ###/g
        var newPagesLine = lineNumber(fixture, newPageEx)[0].line;
        var newRouteLine = lineNumber(fixture, newRouteEx)[0].line;
        
        // when we found the newPagesLine, add text
        if(typeof newPagesLine !== "undefined" && newPagesLine != 0) {
            insertLine(backendRoutesPath).contentSync("import '../../ui/backend/pages/" + collectionName + "/" + collectionName + ".js';").at(newPagesLine);
        }

        // when we found the newRouteLine, add text
        if(typeof newRouteLine !== "undefined" && newRouteLine != 0) 
        {
            var string = [
                "",
                "/**",
                " * " + data.ucCollection,
                " */",
                "",            
                "backendRoutes.route('" + this.options.route + "', {",
                "   name: 'Backend." + data.lcCollection + "',",
                "   subscriptions() {",
                "       this.register('" + data.lcCollection + "', Meteor.subscribe('" + data.lcCollection + ".all'));",
                "   },",
                "   action() {",
                "       var data = { title: '" + data.ucCollection  + "'}",
                "       DocHead.setTitle(data.title + ' // Backend');",
                "       BlazeLayout.render('Backend_admin', { main: '" + data.lcCollection + "', data: { title: '" + data.ucCollection + "' } });",
                "   }",
                "});"
            ]
            insertLine(backendRoutesPath).contentSync(string.join("\n")).at(newRouteLine);
        }
    }
}
/**
 * The add <%=lcCollection%> component
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { <%=ucCollection%> } from '/imports/api/<%=lcCollection%>/<%=lcCollection%>.js';
import { Meteor } from 'meteor/meteor';
import './add<%=collectionSingle%>.html';

// template helper
Add<%=collectionSingleUppercase%> = {
    close: function() {
        $('#add<%=collectionSingleUppercase%>').modal('hide');
    },
    show: function() {
        $('#add<%=collectionSingleUppercase%>').modal('show');
    }
}

// on rendered
Template.add<%=collectionSingle%>.onRendered(() => {
    var modal = $('#add<%=collectionSingleUppercase%>');
    modal.on('hidden.bs.modal', function () {
        modal.find('.txtTitle').val('');
    })
});

// on created
Template.add<%=collectionSingle%>.onCreated(() => {
});

// helpers
Template.add<%=collectionSingle%>.helpers({
});

// events
Template.add<%=collectionSingle%>.events({
    'click .submit': function(e)
    {
        // get the modal        
        var modal = $('#add<%=collectionSingleUppercase%>');

        // define the data object
        var data = {
            title: modal.find('.txtTitle').val()
        }        

        // save the object
        Meteor.call('<%=lcCollection%>.save', data, (err) => {
            if(err) { AdminModal.showError(err.error, err.reason); }
            else {
                Add<%=collectionSingleUppercase%>.close();
                NotifyHelpers.success('New <%=collectionSingle%> added.', 'The <%=collectionSingle%> "' + data.title + '" has been added.');
            }
        });        
    }
});

// destroyed
Template.add<%=collectionSingle%>.onDestroyed(function () {
});
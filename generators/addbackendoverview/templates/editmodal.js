/**
 * The edit users component
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { <%=ucCollection%> } from '/imports/api/<%=lcCollection%>/<%=lcCollection%>.js';
import { Meteor } from 'meteor/meteor';
import './edit<%=collectionSingle%>.html';

Edit<%=collectionSingleUppercase%> = {
    close: function() {
        $('#edit<%=collectionSingleUppercase%>').modal('hide');
    },
    show: function(id) {
        var <%=collectionSingle%>Exists = typeof <%=ucCollection%>.findOne({ _id: id }) !== "undefined";
        if(<%=collectionSingle%>Exists) {
            Session.set('<%=collectionSingle%>', <%=ucCollection%>.findOne({ _id: id }));
            $('#edit<%=collectionSingleUppercase%>').modal('show');
        } else {
            NotifyHelpers.error('<%=collectionSingleUppercase%> error.', 'The <%=collectionSingle%> does not exist.');
        }        
    }
}

// on rendered
Template.edit<%=collectionSingle%>.onRendered(() => {
    var modal = $('#edit<%=collectionSingleUppercase%>');
    modal.on('hidden.bs.modal', function () {
        modal.find('.txtTitle').val('');
    })
});

// on created
Template.edit<%=collectionSingle%>.onCreated(() => {
});

// helpers
Template.edit<%=collectionSingle%>.helpers({
    <%=collectionSingle%>() {
        return Session.get('<%=collectionSingle%>');
    }
});

// events
Template.edit<%=collectionSingle%>.events({
    'click .submit': function(e)
    {
        // get the modal
        var modal = $('#edit<%=collectionSingleUppercase%>');

        // get object
        var <%=collectionSingle%> = Session.get('<%=collectionSingle%>');
        
        // create data
        var data = {
            _id: <%=collectionSingle%>._id,
            title: modal.find('.txtTitle').val()
        }
        
        // save in database
        Meteor.call('<%=lcCollection%>.save', data, (err) => {
            if(err) { AdminModal.showError(err.error, err.reason); }
            else {
                Edit<%=collectionSingleUppercase%>.close();
                NotifyHelpers.success('<%=collectionSingleUppercase%> edited.', 'The <%=collectionSingle%> "' + data.title + '" has been edited.');
            }
        });        
    }
});

// destroyed
Template.add<%=collectionSingle%>.onDestroyed(function () {
});

/**
 * The <%=lcCollection%> template
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Meteor } from 'meteor/meteor';
import { <%=ucCollection%> } from '/imports/api/<%=lcCollection%>/<%=lcCollection%>.js';
import '../../components/<%=lcCollection%>/add<%=collectionSingle%>/add<%=collectionSingle%>.js';
import '../../components/<%=lcCollection%>/edit<%=collectionSingle%>/edit<%=collectionSingle%>.js';
import './<%=lcCollection%>.html';

// on rendered
Template.<%=lcCollection%>.onRendered(function() {
    Session.set("pageTitle", this.data.data().title);
});

// on created
Template.<%=lcCollection%>.onCreated(function() {
});

// helpers
Template.<%=lcCollection%>.helpers({
    pageTitle() {
        return Session.get('pageTitle');
    },
    has<%=ucCollection%>() {
        return <%=ucCollection%>.find().count() > 0;
    },
    <%=lcCollection%>() {
        return <%=ucCollection%>.find({}, { sort: { title: 1 }});
    }
});

// events
Template.<%=lcCollection%>.events({
    'click .add': (ev) => {
        Add<%=collectionSingleUppercase%>.show();
    },

    'click .edit': function(e) {    
        Edit<%=collectionSingleUppercase%>.show(this._id);
    },

    'click .delete': function(e)
    {
        var id = this._id;
        var title = this.title;
        AdminModal.showConfirmation("Delete <%=collectionSingleUppercase%>", 'Do you really want to delete <%=collectionSingle%> "' + title + '"?', function() {
            Meteor.call('<%=lcCollection%>.remove', id, function(err, res) {
                if(err) { AdminModal.showError(err.error, err.reason )}
                else {
                    NotifyHelpers.success('Succesfully removed', 'The <%=collectionSingle%> "' + title + '" was succesfully removed.');
                } 
            });
        });
    }
});

// destroyed
Template.<%=lcCollection%>.onDestroyed(function () {
});
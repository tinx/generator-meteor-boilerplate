/**
 * The <%=ucCollection%> collection
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

/**
 * Define Mongo Collection
 */
export const <%=ucCollection%> = new Mongo.Collection("<%=lcCollection%>");

/**
 * Attach a Schema
 */

var Schemas = {};

// create simpleschema
Schemas.<%=collectionSingleUppercase%> = new SimpleSchema({
    title: {
        type: String,
        optional: false
    },
    addedOn: {
        type:Date,
        optional: false,
        defaultValue: new Date()
    }
});

// attach schema
<%=ucCollection%>.attachSchema(Schemas.<%=collectionSingleUppercase%>);
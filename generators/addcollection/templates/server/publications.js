/**
 * All <%=lcCollection%>-related publications
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Meteor } from 'meteor/meteor';
import { <%=ucCollection%> } from '../<%=lcCollection%>.js';

Meteor.publish('<%=lcCollection%>.all', function () {
  return <%=ucCollection%>.find();
});
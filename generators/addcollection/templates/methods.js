/*
 * Methods related to <%=ucCollection%> collection
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { <%=ucCollection%> } from './<%=lcCollection%>.js';

Meteor.methods({  
  '<%=lcCollection%>.save'(data)
  {
    // validate
    check(data.title, String);

    // get one of the <%=lcCollection%>, if one exists
    var obj = <%=ucCollection%>.findOne({ _id: data._id });

    // validate and save
    if(typeof obj !== 'undefined') {
        <%=ucCollection%>.update({ _id: obj._id }, { $set: data });
    } else {
        <%=ucCollection%>.insert(data);
    }    
  },
  '<%=lcCollection%>.remove'(_id)
  {
    // validate
    check(_id, String);

    // validate and remove
    if(typeof _id != "undefined") {
        <%=ucCollection%>.remove({ _id: _id });
    }
  }
});

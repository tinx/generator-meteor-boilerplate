'use strict';
var Generator = require('yeoman-generator');
var fs = require('fs');
var chalk = require('chalk');

module.exports = class extends Generator {

    constructor(args, opts) 
    {
        super(args, opts);

        try
        {
            this.argument('name', {
                type: String,
                required: true,
                description: 'Collection name'
            });
        }
        catch(ex) 
        {
            this.log(chalk.red('CREATE COLLECTION...Please provide a valid collection name argument.'));
        }
    }

    writing() 
    {
        // validate
        if(this.options.name == "" || typeof this.options.name == "undefined") return;
        
        // get the collection name
        var collectionName = this.options.name.toLowerCase();
        var collectionNameUppercase = collectionName.charAt(0).toUpperCase() + collectionName.slice(1);
        var collectionSingle = collectionName.slice(0, -1);
        var collectionSingleUppercase = collectionSingle.charAt(0).toUpperCase() + collectionSingle.slice(1);

        // the collection path
        var collectionPath = process.cwd() + "/imports/api/" + collectionName + "/";
        var collectionServerPath = collectionPath + "/server";
        var registerApiPath = process.cwd() + "/imports/startup/server/register-api.js";

        // create the collection directories
        if (!fs.existsSync(collectionPath)){ fs.mkdirSync(collectionPath); }
        else {  
            this.log(chalk.red('Collection already exists.'));
            return;
        }
        
        // create the template data
        var templateData = {
            lcCollection: collectionName,
            ucCollection: collectionNameUppercase,
            collectionSingle: collectionSingle,
            collectionSingleUppercase: collectionSingleUppercase
        }

        // copy files for publication
        this.fs.copyTpl(
            this.templatePath('server/publications.js'),
            this.destinationPath(collectionPath + "/server/publications.js"),
            templateData
        );

        // copy files for methods
        this.fs.copyTpl(
            this.templatePath('methods.js'),
            this.destinationPath(collectionPath + "/methods.js"),
            templateData
        );

        // copy files for collection
        this.fs.copyTpl(
            this.templatePath('collection.js'),
            this.destinationPath(collectionPath + "/" + collectionName + ".js"),
            templateData
        );

        // register the api
        var string = [
            "",
            "",
            "/**",
            " * " + collectionNameUppercase,
            " */",
            "",            
            "import '../../api/" + collectionName + "/methods.js';",
            "import '../../api/" + collectionName + "/server/publications.js';"
        ]

        // append to file
        fs.appendFileSync(registerApiPath, string.join("\n"), {mode:777});
    }

};
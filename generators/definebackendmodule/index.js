'use strict';
var Generator = require('yeoman-generator');
var jsonFile = require('jsonfile');
var fs = require('fs');
var chalk = require('chalk');

module.exports = class extends Generator {

    constructor(args, opts) 
    {
        super(args, opts);

        try
        {
            this.argument('name', {
                type: String,
                required: true,
                description: 'Module name'
            });
        }
        catch(ex) 
        {
            this.log(chalk.red('CREATE MODULE...Please provide a valid module name argument.'));
        }
    }

    writing()
    {
        // validate
        if(this.options.name == "" || typeof this.options.name === "undefined") return;

        // get the module name
        var moduleName = this.options.name.toLowerCase();
        var moduleNameUppercase = moduleName.charAt(0).toUpperCase() + moduleName.slice(1)
        var jsonPath = process.cwd() + "/private/modules.json";

        // validate
        if(fs.existsSync(jsonPath))
        {            
            // get the json file
            var json = jsonFile.readFileSync(jsonPath);
            
            // add to json file
            json.push({
                "name": moduleName,
                "path": "/admin/modules/" + moduleName,
                "label": moduleNameUppercase
            });

            // write to system
            jsonFile.writeFileSync(jsonPath, json, {spaces: 2, EOL: '\r\n'});
        }
    }

}
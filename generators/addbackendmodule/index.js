'use strict';
var Generator = require('yeoman-generator');

module.exports = class extends Generator {
    
    prompting() 
    {
        // prompt something
        return this.prompt([
            {
                type: 'input',
                name: 'name',
                message: 'The name of the module // use plural format (e.g. Devices)',
                default: 'Defaults'
            }
        ]).then((props) => {
            this.props = props;
        });
    }
    
    install() 
    {
        // create a new collection
        this.composeWith(require.resolve('../addcollection'), { arguments: [
            this.props.name
        ]});

        // create a new module
        this.composeWith(require.resolve('../definebackendmodule'), { arguments: [
            this.props.name
        ]});
        
        // create the backend overview
        this.composeWith(require.resolve('../addbackendoverview'), { arguments: [
            this.props.name,
            "/modules/" + this.props.name
        ]});
    }

};